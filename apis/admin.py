from django.contrib import admin
from apis.employees.models import Employee
from apis.reviews.models import Review

# Register your models here.
admin.site.register(Employee)
admin.site.register(Review)