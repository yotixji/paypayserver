from datetime import datetime
import uuid
from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.timezone import now

class Employee(models.Model):
    DEPARTMENT_CHOICES = [
        ('software', 'Software'),
        ('security', 'Security'),
        ('human resources', 'Human Resource'),
        ('account', 'Account'),
        ('finance', 'Finance')
    ]
    name = models.CharField(max_length=80)
    email = models.EmailField(max_length=100, unique=True)
    department = models.CharField(max_length=80, choices=DEPARTMENT_CHOICES)
    date_of_joining = models.DateTimeField(default=now, blank=True)

    def __str__(self):
        return 'Employee: ' + self.email + ", Dept: " + self.department 

@receiver(post_save, sender=Employee, dispatch_uid='create_new_user')
def create_user(sender, instance, **kwargs):
    try:
        if User.objects.get(username="paypay"+str(instance.id)):
            return
    except User.DoesNotExist:
        user = User.objects.create_user("paypay"+str(instance.id), email=instance.email,password="paypay"+str(instance.id))
        user.save()
