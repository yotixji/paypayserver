from django.contrib.auth.models import User
from rest_framework import serializers
from apis.employees.models import Employee

class EmployeeSerializers(serializers.ModelSerializer):
    class Meta:
        model = Employee
        fields = ['id', 'name', 'email', 'department', 'date_of_joining']
