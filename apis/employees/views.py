from rest_framework import generics, status
from rest_framework.permissions import BasePermission, IsAdminUser, IsAuthenticated, SAFE_METHODS
from rest_framework.authentication import TokenAuthentication
from rest_framework.response import Response
from apis.employees.models import Employee
from apis.employees.serializers import EmployeeSerializers

class ReadOnly(BasePermission):
    def has_permission(self, request, view):
        if request.method in SAFE_METHODS and request.user and request.user.is_authenticated:
            return True
        return False

class EmployeeList(generics.ListCreateAPIView):
    queryset = Employee.objects.all()
    serializer_class = EmployeeSerializers
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAdminUser|ReadOnly]

    def list(self, request):
        queryset = self.get_queryset()
        serializer = EmployeeSerializers(queryset, many=True)
        return Response(serializer.data)

    def create(self, request):
        serializer = EmployeeSerializers(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class EmployeeDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Employee.objects.all()
    serializer_class = EmployeeSerializers
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
