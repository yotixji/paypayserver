from datetime import datetime
import uuid
from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone

# Create your models here.

class BaseModel(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    date_created = models.DateTimeField(default=timezone.now())
    date_modified = models.DateTimeField(default=timezone.now())
