from datetime import datetime
import uuid
from django.contrib.auth.models import User
from django.db import models
from apis.employees.models import Employee
from apis.validators import REVIEW_VALUE_VALIDATOR
from django.core.exceptions import ValidationError


class Review(models.Model):
    quality = models.IntegerField(null=False, validators=REVIEW_VALUE_VALIDATOR)
    attitude = models.IntegerField(null=False, validators=REVIEW_VALUE_VALIDATOR)
    commitment = models.IntegerField(null=False, validators=REVIEW_VALUE_VALIDATOR)
    productivity = models.IntegerField(null=False, validators=REVIEW_VALUE_VALIDATOR)
    knowledge =models.IntegerField(null=False, validators=REVIEW_VALUE_VALIDATOR)
    quality_text = models.CharField(max_length=250, blank=True)
    attitude_text = models.CharField(max_length=250, blank=True)
    commitment_text = models.CharField(max_length=250, blank=True)
    productivity_text = models.CharField(max_length=250, blank=True)
    knowledge_text = models.CharField(max_length=250, blank=True)
    employee = models.ForeignKey(Employee, on_delete=models.CASCADE, null=False, related_name='reviewfor')
    review_by = models.ForeignKey(Employee, on_delete=models.CASCADE, null=False, related_name='reviewby')
    completed = models.IntegerField(default=0)

    def clean(self):
        if self.employee == self.review_by:
            raise ValidationError('Employee and reviewer must not be same')