from django.contrib.auth.models import User
from rest_framework import serializers
from apis.employees.models import Employee
from apis.reviews.models import Review

class ReviewSerializers(serializers.ModelSerializer):
    employee = Employee

    class Meta:
        depth = 1
        model = Review
        fields = ['id', 'quality', 'quality_text', 'attitude', 'attitude_text',
                    'commitment', 'commitment_text', 'productivity', 
                    'productivity_text', 'knowledge', 'completed', 'knowledge_text', 'employee']