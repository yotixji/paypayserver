from apis.employees.models import Employee
from django.shortcuts import render
from django.contrib.auth.models import User
from rest_framework import generics, status
from rest_framework.permissions import BasePermission, IsAdminUser, IsAuthenticated, SAFE_METHODS
from rest_framework.authentication import TokenAuthentication
from rest_framework.response import Response
from apis.reviews.models import Review
from apis.reviews.serializers import ReviewSerializers

class ReadOnly(BasePermission):
    def has_permission(self, request, view):
        if request.method in SAFE_METHODS and request.user and request.user.is_authenticated:
            return True
        return False

class ReviewList(generics.ListCreateAPIView):
    queryset = Review.objects.all()
    serializer_class = ReviewSerializers
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        user = self.request.user
        user = Employee.objects.get(email=user.email)
        queryset = Review.objects.filter(review_by=user, completed=0)
        return queryset

    def list(self, request):
        queryset = self.get_queryset()
        serializer = ReviewSerializers(queryset, many=True)
        return Response(serializer.data)

    def create(self, request):
        serializer = ReviewSerializers(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ReviewDetail(generics.RetrieveUpdateAPIView):
    queryset = Review.objects.all()
    serializer_class = ReviewSerializers
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
