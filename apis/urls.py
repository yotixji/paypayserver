from apis.views import LoginView, UserDetail
from django.urls import path
from apis.reviews.views import ReviewList, ReviewDetail
from apis.employees.views import EmployeeList, EmployeeDetail

urlpatterns = [
    path('profile/<int:pk>',UserDetail.as_view(),name='user-profile'),
    path('reviews', ReviewList.as_view(), name='review-list'),
    path('reviews/<int:pk>', ReviewDetail.as_view(), name='review-detail'),
    path('employees', EmployeeList.as_view(), name='employees-list'),
    path('employees/<int:pk>', EmployeeDetail.as_view(), name='employees-detail'),
    path('login', LoginView.as_view(), name='login')
    
]