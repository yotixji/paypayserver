from django.core.validators import MinValueValidator, MaxValueValidator
from apis.consts.errors import REVIEW_VALIDATION

REVIEW_VALUE_VALIDATOR=(MinValueValidator(1,REVIEW_VALIDATION), MaxValueValidator(4,REVIEW_VALIDATION ))